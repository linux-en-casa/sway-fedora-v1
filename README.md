# Sway Window Manager en Fedora 40 Atomic - Sway - Sericea

Archivos de Configuración *dot\-files* del Gestor de Ventanas tipo Mosaico **Sway WM**.  

![Image](./imagenes/sway-01.png)

---


## Aplicaciones utilizadas

* Barra superior con **Waybar**.
* **Rofi** y **Wofi**, para lanzar aplicaciones.
* **Wlogout**, para Salir - Reiniciar - Apagar.
* **foot**, el emulador de terminal.
* **sway**, Gestor de Ventanas, Tiling Window Manager.

![Image](./imagenes/sway-02.png)
![Image](./imagenes/sway-03.png)


---


## Instalación - Configuración

La instalación y configuración inicial la explico (paso a paso) en:  
[YouTube, LeC - Fedora 40 con Sway WM](https://youtu.be/NoXSthaJqlI) .  

También hice varios videos y directos que reuno en:  
[Sway WM - Directos y Videos en YouTube](https://www.youtube.com/playlist?list=PLB4ymvNB9_0YVLybMmqx5NZm2qenBLYdC) 

---

## Detalles - Notas

Patiendo de la configuración inicial que trae la Versión Atómica de Fedora Sway 40, hice los siguientes cambios:

1. Aumentar tamaño de la letra del emulador de terminal foot a 15.
2. Instalar y configurar wofi para lanzar aplicaciones.
    * El atajo de teclado de wofi es: ***Super \+ Alt \+ d***.
    * Rofi sigue funcionando con ***Super \+ d***, pero no lo configuré.
3. Instalar wlogout para Salir - Reiniciar - Apagar.
    * El atajo de teclado es ***Super \+ Shift \+ x***.
4. Agregué 2 módulos a la barra de waybar para lanzar tanto wofi como wlogout.
    * Módulo **custom/menu**, para lanzar wofi.
    * Módulo **custom/power**, para lanzar wlogout.
5. Algunos otros cambios en el archivo de configuración de Sway.
    * Teclado en español, línea 72.
    * Tiempo del bloqueo de pantalla, línea 48.
    * Y los atajos de wlogout y wofi, al final del archivo.

![Image](./imagenes/sway-04.png)

---

Para mayores detalles de la instalación y configuración, pueden consultar los videos en:  
[YouTube, Linux en Casa](https://www.youtube.com/@LinuxenCasa/videos) .  

---


**GNU General Public License v3.0**

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.



